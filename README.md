###专门为餐饮店定制的一款后台员工管理以及小程序用户使用的软件产品

**注意：基于B站“黑马程序员”讲解的基础进行搭建，编写，笔记仅供参考、学习，旨在相互探讨**
**该笔记的图片链接能够直接在git上面直接浏览，若想下载后浏览，需要将链接前面的http更换成本地地址**

# 一、项目背景介绍

![](https://gitee.com/stone_wlh/fhlou/raw/master/assert/项目介绍.png)


笔记内容：

1. 基于后台管理的代码实现业务的http请求即controller方法参数的编写
2. 基于移动端的代码实现业务的http请求即controller方法参数的编写
3. 项目访问地址：
   1. 后台：http://localhost:8086/backend/page/login/login.html
   2. 移动端：http://localhost:8086/front/page/login.html

# 二、后台管理请求

#### 1、Employee业务请求

1. ##### 登录

   ##### ![](https://gitee.com/stone_wlh/fhlou/raw/master/assert/login.png)

   ```java
   @PostMapping("/login")
   public R<Employee> login(HttpServletRequest request, @RequestBody Employee employee)
   ```

   

2. ##### 退出登录

   ##### ![](https://gitee.com/stone_wlh/fhlou/raw/master/assert/loginOut.png)

   ```java
   @PostMapping("/logout")
   public R<String> logout(HttpServletRequest request)
   ```

   

3. ##### 员工分页查询

   ##### ![](https://gitee.com/stone_wlh/fhlou/raw/master/assert/page_employee.png)

   ```java
   @GetMapping("/page")
   public R<Page> pageQuery(int page, int pageSize, String name)
   ```

   

4. ##### 新增员工

   ##### ![](https://gitee.com/stone_wlh/fhlou/raw/master/assert/add_employee.png)

   ```java
   @PostMapping
   public R<String> save(HttpServletRequest request, @RequestBody Employee employee)
   ```

   

5. ##### 修改员工信息、修改员工状态

   ##### ![](https://gitee.com/stone_wlh/fhlou/raw/master/assert/update_employ.png)

   ```java
   @PutMapping
   public R<String> update(HttpServletRequest request, @RequestBody Employee employee)
   ```

   

6. ##### 修改员工时的信息回显

   ##### ![](https://gitee.com/stone_wlh/fhlou/raw/master/assert/update_employ_huixian.png)

   ```java
   @GetMapping("/{id}")
   public R<Employee> getById(@PathVariable("id") Long id)
   ```

#### 2、Category业务请求

1. ##### 新增分类项

   ##### ![](https://gitee.com/stone_wlh/fhlou/raw/master/assert/add_category.png)

   ```java
   @PostMapping
   public R<String> save(@RequestBody Category category)
   ```

   

2. ##### 分类项分页查询

   ##### ![](https://gitee.com/stone_wlh/fhlou/raw/master/assert/page_category.png)

   ```java
   @GetMapping("/page")
   public R<Page> pageQuery(int page, int pageSize)
   ```

   

3. ##### 删除分类项

   ##### ![](https://gitee.com/stone_wlh/fhlou/raw/master/assert/delete_category.png)

   ```java
   @DeleteMapping
   public R<String> deleteCategory(@RequestParam Long ids)
   ```

   

4. ##### 修改分类项

   ##### ![](https://gitee.com/stone_wlh/fhlou/raw/master/assert/update_category.png)

   ```java
   @PutMapping
   public R<String> updateCategory(@RequestBody Category category)
   ```

   

5. ##### 菜品/套餐操作数据时，分类项的查询

   ##### ![](https://gitee.com/stone_wlh/fhlou/raw/master/assert/page_category_byothers.png)

   ```java
   @GetMapping("/list")
   public R<List<Category>> getList(Category category)
   ```



#### 3、Dish业务请求

1. ##### 新增菜品

   ##### ![](https://gitee.com/stone_wlh/fhlou/raw/master/assert/add_dish.png)

   ```java
   @PostMapping
   public R<String> save(@RequestBody DishDto dishDto)
   ```

   

2. ##### 菜品信息分页

   ##### ![](https://gitee.com/stone_wlh/fhlou/raw/master/assert/page_dish.png)

   ```java
   @GetMapping("/page")
   public R<Page> pageQuery(int page, int pageSize, String name)
   ```

   

3. ##### 修改菜品时回显数据

   ##### ![](https://gitee.com/stone_wlh/fhlou/raw/master/assert/update_dish_huixian.png)

   ```java
   @GetMapping("/{id}")
   public R<DishDto> queryList(@PathVariable Long id)
   ```

   

4. ##### 修改菜品

   ##### ![](https://gitee.com/stone_wlh/fhlou/raw/master/assert/update_dish.png)

   ```java
   @PutMapping
   public R<String> updateDish(@RequestBody DishDto dishDto)
   ```

   

5. ##### 删除菜品/批量

   ##### ![](https://gitee.com/stone_wlh/fhlou/raw/master/assert/delete_dish.png)

   ```java
   @DeleteMapping
   public R<String> deleteDish(@RequestParam("ids") List<Long> ids)
   ```

   

6. ##### 更改菜品状态

   ##### ![](https://gitee.com/stone_wlh/fhlou/raw/master/assert/update_dish_status.png)

   ```java
   @PostMapping("/status/{status}")
   public R<String> updateDishStatus(@PathVariable("status") Integer status, @RequestParam List<Long> ids)
   ```

   

7. ##### 操作套餐数据时，查询菜品数据

   ##### ![](https://gitee.com/stone_wlh/fhlou/raw/master/assert/querysetmealOnDIsh.png)

   ```java
   @GetMapping("/list") // 初始后台业务
   public R<List<Dish>> listDish(Dish dish)
   ```

   

#### 4、Setmeal业务请求

1. ##### 新增套餐

   ##### ![](https://gitee.com/stone_wlh/fhlou/raw/master/assert/add_setmeal.png)

   ```java
   @PostMapping
   public R<String> saveSetmeal(@RequestBody SetmealDto setmealDto)
   ```

   

2. ##### 套餐分页查询

   ##### ![](https://gitee.com/stone_wlh/fhlou/raw/master/assert/page_setmeal.png)

   ```java
   @GetMapping("/page")
   public R<Page> pageSetmeal(int page, int pageSize, String name)
   ```

   

3. ##### 删除套餐/批量

   ##### ![](https://gitee.com/stone_wlh/fhlou/raw/master/assert/delete_setmeal.png)

   ```java
   @DeleteMapping
   public R<String> deleteOrALl(@RequestParam List<Long> ids)
   ```

   

4. ##### 套餐的单个启停售/批量![](https://gitee.com/stone_wlh/fhlou/raw/master/assert/update_setmeal_status.png)

   ```java
   @PostMapping("/status/{status}")
   public R<String> updateStatus(@PathVariable("status") Integer status, @RequestParam List<Long> ids)
   ```

   

5. ##### 修改套餐时回显数据

   ##### ![](https://gitee.com/stone_wlh/fhlou/raw/master/assert/update_setmeal_huixian.png)

   ```java
   @GetMapping("/{id}")
   public R<SetmealDto> querySetmeal(@PathVariable Long id)
   ```

   

6. ##### 后台修改套餐数据

   ##### ![](https://gitee.com/stone_wlh/fhlou/raw/master/assert/update_setmeal.png)

   ```java
   @PutMapping
   public R<String> updateSetmeal(@RequestBody SetmealDto setmealDto)
   ```

   

#### 5、Orders请求

1. ##### 订单分页查询

   ![](https://gitee.com/stone_wlh/fhlou/raw/master/assert/page_order_houtai.png)

   ```java
   @GetMapping("/page")
   public R<Page> pageQuery(int page, int pageSize, String number, String beginTime, String endTime)
   ```

   

2. ##### 后台管理员修改订单状态

   ##### ![](https://gitee.com/stone_wlh/fhlou/raw/master/assert/update_orders_status_houtai.png)

   ```java
   @PutMapping
   public R<String> updateOrderStatus(@RequestBody Map<String,String> orderMap)
   ```



# 三、移动端请求

#### 1、Category请求

1. ##### 菜品/套餐操作数据时，分类项的查询

   ##### ![](https://gitee.com/stone_wlh/fhlou/raw/master/assert/dish_list_yidong.png)

   ```java
   @GetMapping("/list")
   public R<List<Category>> getList(Category category)
   ```



#### 2、Dish业务请求

1. ##### 操作套餐数据时，查询菜品数据

   ##### ![](https://gitee.com/stone_wlh/fhlou/raw/master/assert/querysetmealOnDIsh.png)

   ```java
   @GetMapping("/list") // 改造，适用于后台及移动端
       public R<List<DishDto>> listDish(Dish dish)
   ```

   

#### 3、Setmeal业务请求

1. ##### 移动端查询套餐

   ##### ![](https://gitee.com/stone_wlh/fhlou/raw/master/assert/yidong_setmeal_list.png)

   ```java
   @GetMapping("/list")
   public R<List<Setmeal>> listSetmeal(Setmeal setmeal)
   ```

   

2. ##### 移动端查询套餐中的菜品信息

   ##### ![](https://gitee.com/stone_wlh/fhlou/raw/master/assert/yidong_dishonsetmeal.png)

   ```java
   @GetMapping("/dish/{setmealId}")
   public R<List<DishDto>> listDishOnSetmeal(@PathVariable Long setmealId)
   ```



#### 4、Orders请求

1. ##### 提交订单

   ##### ![](https://gitee.com/stone_wlh/fhlou/raw/master/assert/yidong_submitorder.png)

   ```java
   @PostMapping("/submit")
   public R<String> submitOrders(@RequestBody Orders orders)
   ```

   

2. ##### 移动端订单分页查询

   ##### ![](https://gitee.com/stone_wlh/fhlou/raw/master/assert/yidong_orderpage.png)

   ```java
   @GetMapping("/userPage")
   public R<Page> pageQuery(int page, int pageSize)
   ```

   

3. ##### 再来一单

   ##### ![](https://gitee.com/stone_wlh/fhlou/raw/master/assert/orderagain.png)

   ```java
   @PostMapping("/again")
   public R<String> againSubmit(@RequestBody Map<String, String> map)
   ```



#### 5、User业务请求

1. ##### 发送验证码

   ##### ![](https://gitee.com/stone_wlh/fhlou/raw/master/assert/user_sendMsg.png)

   ```java
   @PostMapping("/sendMsg")
   public R<String> sendMsg(@RequestBody User user, HttpSession httpSession)
   ```

   

2. ##### 用户登录

   ##### ![](https://gitee.com/stone_wlh/fhlou/raw/master/assert/userlogin.png)

   ```java
   @PostMapping("/login")
   public R<User> login(@RequestBody Map userMap, HttpSession httpSession)
   ```

   

3. ##### 用户退出登录

   ##### ![](https://gitee.com/stone_wlh/fhlou/raw/master/assert/userloginout.png)

   ```java
   @PostMapping("/loginout")
   public R<String> loginOut(HttpServletRequest httpRequest)
   ```



#### 6、ShoppingCart业务请求

1. ##### 添加美食到购物车

   ##### ![](https://gitee.com/stone_wlh/fhlou/raw/master/assert/shoppingcart/addShoppcart.png)

   ```java
   @PostMapping("/add")
   public R<ShoppingCart> addShoppingCart(@RequestBody ShoppingCart shoppingCart)
   ```

   

2. ##### 查看购物车

   ##### ![](https://gitee.com/stone_wlh/fhlou/raw/master/assert/shoppingcart/listshoppingcart.png)

   ```java
   @GetMapping("/list")
   public R<List<ShoppingCart>> listShoppingCart()
   ```

   

3. ##### 清空当前购物车

   ##### ![](https://gitee.com/stone_wlh/fhlou/raw/master/assert/shoppingcart/cleancart.png)

   ```java
   @DeleteMapping("/clean")
   public R<String> cleanShoppingCart()
   ```

   

4. ##### 减少当前购物车美食

   ##### ![](https://gitee.com/stone_wlh/fhlou/raw/master/assert/shoppingcart/subcart.png)

   ```java
   @PostMapping("/sub")
   public R<String> subDishOrSetmeal(@RequestBody ShoppingCart shoppingCart)
   ```



#### 7、AddressBook业务请求

1. ##### 查询指定用户的全部地址

   ##### ![](https://gitee.com/stone_wlh/fhlou/raw/master/assert/shoppingcart/address_list.png)

   ```java
   @GetMapping("/list")
   public R<List<AddressBook>> list(AddressBook addressBook)
   ```

   

2. ##### 新增地址

   ##### ![](https://gitee.com/stone_wlh/fhlou/raw/master/assert/shoppingcart/address_add.png)

   ```java
   @PostMapping
   public R<AddressBook> save(@RequestBody AddressBook addressBook)
   ```

   

3. ##### 设置默认地址

   ##### ![](https://gitee.com/stone_wlh/fhlou/raw/master/assert/shoppingcart/address_default.png)

   ```java
   @PutMapping("default")
   public R<AddressBook> setDefault(@RequestBody AddressBook addressBook)
   ```

   

4. ##### 根据id查询地址

   ##### ![](https://gitee.com/stone_wlh/fhlou/raw/master/assert/shoppingcart/address_byid.png)

   ```java
   @GetMapping("/{id}")
   public R get(@PathVariable Long id)
   ```

   

5. ##### 查询默认地址

   ##### ![](https://gitee.com/stone_wlh/fhlou/raw/master/assert/shoppingcart/address_getdefault.png)

   ```java
   @GetMapping("default")
   public R<AddressBook> getDefault()
   ```

   

6. ##### 修改地址

   ##### ![](https://gitee.com/stone_wlh/fhlou/raw/master/assert/shoppingcart/updateaddress.png)

   ```
   @PutMapping
   public R<String> updateAddress(@RequestBody AddressBook addressBook)
   ```

   

7. ##### 删除地址

   ##### ![](https://gitee.com/stone_wlh/fhlou/raw/master/assert/shoppingcart/deleteaddress.png)

   ```
   @DeleteMapping
   public R<String> deleteAddress(@RequestParam("ids") Long ids)
   ```



# 四、总结

​		基于“黑马程序员”的瑞吉外卖项目，本人实现了视频中所讲的一些功能并且也将个人实现部分完成了，前端业务也修改了一些，可能代码不是很规范，并且我个人认为一些业务上的逻辑不是很正确，但是这些单体项目，数据库设计还有一些前端页面的限制，很难进行公司上的开发，并由于个人实力以及时间问题，并没有进行更多的优化，但希望有兴趣的小伙伴能够进行相互的探讨交流，创建一个和谐的码农空间。

​		该笔记只是展示了各种业务请求的参数处理，不唯一，更多的代码详情可以浏览源码。