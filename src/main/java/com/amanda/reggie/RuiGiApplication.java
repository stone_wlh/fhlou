package com.amanda.reggie;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author amanda
 * @Description 启动类
 */
@Slf4j
@SpringBootApplication
@ServletComponentScan
@EnableTransactionManagement
public class RuiGiApplication {
    public static void main(String[] args) {
        SpringApplication.run(RuiGiApplication.class, args);
        log.info("RuiGi项目启动成功...");
    }
}
