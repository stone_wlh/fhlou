package com.amanda.reggie.mapper;

import com.amanda.reggie.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author amanda
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {
}
