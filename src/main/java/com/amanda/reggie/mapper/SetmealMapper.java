package com.amanda.reggie.mapper;

import com.amanda.reggie.entity.Setmeal;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author amanda
 */
@Mapper
public interface SetmealMapper extends BaseMapper<Setmeal> {
}
