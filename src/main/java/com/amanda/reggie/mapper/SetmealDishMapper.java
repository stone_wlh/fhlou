package com.amanda.reggie.mapper;

import com.amanda.reggie.entity.SetmealDish;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author amanda
 */
@Mapper
public interface SetmealDishMapper extends BaseMapper<SetmealDish> {
}
