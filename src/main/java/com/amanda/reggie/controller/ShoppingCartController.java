package com.amanda.reggie.controller;

import com.amanda.reggie.common.R;
import com.amanda.reggie.entity.ShoppingCart;
import com.amanda.reggie.service.ShoppingCartService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author amanda
 * @Description 购物车管理
 */
@RestController
@Slf4j
@RequestMapping("/shoppingCart")
public class ShoppingCartController {

    @Autowired
    private ShoppingCartService shoppingCartService;

    /**
     * 添加美食到购物车
     * @param shoppingCart 购物车
     * @return R<ShoppingCart>
     */
    @PostMapping("/add")
    public R<ShoppingCart> addShoppingCart(@RequestBody ShoppingCart shoppingCart) {
        return R.success(shoppingCartService.addShoppingCart(shoppingCart));
    }

    /**
     * 查看购物车
     * @return R<List<ShoppingCart>>
     */
    @GetMapping("/list")
    public R<List<ShoppingCart>> listShoppingCart() {
        return R.success(shoppingCartService.listShoppingCart());
    }

    /**
     * 清空当前购物车
     * @return R<String>
     */
    @DeleteMapping("/clean")
    public R<String> cleanShoppingCart() {
        shoppingCartService.cleanShoppingCart();
        return R.success("清空购物车成功！");
    }

    /**
     * 减少当前购物车的美食
     * @param shoppingCart 购物数据
     * @return R<String>
     */
    @PostMapping("/sub")
    public R<String> subDishOrSetmeal(@RequestBody ShoppingCart shoppingCart) {
        return shoppingCartService.subDishOrSetmeal(shoppingCart);
    }

}
