package com.amanda.reggie.controller;

import com.amanda.reggie.common.R;
import com.amanda.reggie.dto.DishDto;
import com.amanda.reggie.dto.SetmealDto;
import com.amanda.reggie.entity.Setmeal;
import com.amanda.reggie.service.SetmealService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author amanda
 * @Description 套餐管理
 */
@Slf4j
@RestController
@RequestMapping("/setmeal")
public class SetmealController {

    @Autowired
    private SetmealService setmealService;

    /**
     * 新增套餐
     * @param setmealDto 套餐数据
     * @return R<String>
     */
    @PostMapping
    public R<String> saveSetmeal(@RequestBody SetmealDto setmealDto) {
        setmealService.saveSetmealWithDish(setmealDto);
        return R.success("新增套餐成功！");
    }

    /**
     * 分页查询套餐信息
     * @param page 页码
     * @param pageSize 条数
     * @param name 套餐名称
     * @return R<Page>
     */
    @GetMapping("/page")
    public R<Page> pageSetmeal(int page, int pageSize, String name) {
        return R.success(setmealService.pageSetmeal(page, pageSize, name));
    }

    /**
     * 删除套餐/批量套餐
     * @param ids 套餐id
     * @return R<String>
     */
    @DeleteMapping
    public R<String> deleteOrALl(@RequestParam List<Long> ids) {
        setmealService.removeSetmeal(ids);
        return R.success("删除套餐数据成功！");
    }

    /**
     * 套餐的单个启停售/批量处理
     * @param status 状态码
     * @param ids 套餐id
     * @return R<String>
     */
    @PostMapping("/status/{status}")
    public R<String> updateStatus(@PathVariable("status") Integer status, @RequestParam List<Long> ids) {
        setmealService.updateSetmealStatus(status, ids);
        return R.success("更改套餐状态成功！");
    }

    /**
     * 修改套餐时回显数据
     * @param id 套餐id
     * @return R<SetmealDto>
     */
    @GetMapping("/{id}")
    public R<SetmealDto> querySetmeal(@PathVariable Long id) {
        return R.success(setmealService.querySetmeal(id));
    }

    /**
     * 后台修改套餐数据
     * @param setmealDto 前端套餐数据
     * @return R<String>
     */
    @PutMapping
    public R<String> updateSetmeal(@RequestBody SetmealDto setmealDto) {
        setmealService.updateSetmeal(setmealDto);
        return R.success("修改套餐信息成功！");
    }

    /**
     * 移动端查询套餐
     * @param setmeal 套餐信息
     * @return R<List<Setmeal>>
     */
    @GetMapping("/list")
    public R<List<Setmeal>> listSetmeal(Setmeal setmeal) {
        return R.success(setmealService.listSetmeal(setmeal));
    }


    /**
     * 移动端查询套餐中的菜品信息
     * @param setmealId 套餐id
     * @return R<List<DishDto>>
     */
        @GetMapping("/dish/{setmealId}")
        public R<List<DishDto>> listDishOnSetmeal(@PathVariable Long setmealId) {
        return R.success(setmealService.listDishOnSetmeal(setmealId));
    }

}
