package com.amanda.reggie.controller;

import com.amanda.reggie.common.R;
import com.amanda.reggie.entity.Employee;
import com.amanda.reggie.service.EmployeeService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;


/**
 * @author amanda
 * @Description 员工管理
 */
@Slf4j
@RestController
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    /**
     * 员工登录
     * select * from employee where username = ''
     * @param request 获取session
     * @param employee 员工
     * @return R<Employee>
     */
    @PostMapping("/login")
    public R<Employee> login(HttpServletRequest request, @RequestBody Employee employee) {
        return employeeService.login(request, employee);
    }

    /**
     * 员工退出
     * @param request 清除session
     * @return R<String>
     */
    @PostMapping("/logout")
    public R<String> logout(HttpServletRequest request) {
        // 清除Session
        request.getSession().removeAttribute("employee");
        return R.success("退出当前账户！");
    }

    /**
     * 新增员工
     * insert into employee (列名) values (值)
     * @param request 获取session
     * @param employee 员工
     * @return R<String>
     */
    @PostMapping
    public R<String> save(HttpServletRequest request, @RequestBody Employee employee) {

        // 1、设置初始密码，进行md5加密
        employee.setPassword(DigestUtils.md5DigestAsHex("YANZM0810".getBytes()));

        // 这里使用元数据对象处理器统一填充数据
        // 2、系统时间
        //employee.setCreateTime(LocalDateTime.now());
        //employee.setUpdateTime(LocalDateTime.now());
        // 3、获得当前用户id
        //Long empId = (Long) request.getSession().getAttribute("employee");
        //employee.setCreateUser(empId);
        //employee.setUpdateUser(empId);

        // 这里的异常捕获使用全局异常处理器
        /*
        try {
            // 4、mp保存到数据库
            employeeService.save(employee);
        }catch (Exception e) {
            R.error("新增员工失败！");
        }*/
        employeeService.save(employee);
        return R.success("添加员工成功！");
    }

    /**
     * 员工信息分页查询
     * 改进sql：select * from employee where name = "" or username = "" limit firstIndex, pageSize order by update_time desc
     * @param page 页数
     * @param pageSize 条数
     * @param name 姓名/账号（改进）
     * @return R<Page>
     */
    @GetMapping("/page")
    public R<Page> pageQuery(int page, int pageSize, String name) {
        return R.success(employeeService.pageQuery(page, pageSize, name));
    }

    /**
     * 修改员工信息
     * update employee set () values ()
     * @param request 获取session
     * @param employee 员工
     * @return R<String>
     */
    @PutMapping
    public R<String> update(HttpServletRequest request, @RequestBody Employee employee) {
        // 这里使用元数据对象处理器统一填充数据
        //Long empId = (Long) request.getSession().getAttribute("employee");
        //employee.setUpdateTime(LocalDateTime.now());
        //employee.setUpdateUser(empId);
        employeeService.updateById(employee);
        return R.success("员工信息修改成功！");
    }

    /**
     * 修改员工信息时，根据id回显员工信息
     * select * from employee where id = ''
     * @param id 员工id
     * @return R<Employee>
     */
    @GetMapping("/{id}")
    public R<Employee> getById(@PathVariable("id") Long id) {
        Employee employee = employeeService.getById(id);
        if (null != employee) {
            return R.success(employee);
        }
        return R.error("没有查询到当前员工信息！");
    }
}
