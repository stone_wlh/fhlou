package com.amanda.reggie.controller;

import com.amanda.reggie.common.R;
import com.amanda.reggie.entity.User;
import com.amanda.reggie.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * @author amanda
 * @Description 用户管理
 */
@RestController
@Slf4j
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * 发送验证码
     * @param user 用户
     * @param httpSession session存储code
     * @return R<String>
     */
    @PostMapping("/sendMsg")
    public R<String> sendMsg(@RequestBody User user, HttpSession httpSession) {
        return userService.sendMsg(user, httpSession);
    }

    /**
     * 移动端登录
     * @param userMap 用户信息
     * @param httpSession 服务器存储session
     * @return R<String>
     */
    @PostMapping("/login")
    public R<User> login(@RequestBody Map userMap, HttpSession httpSession) {
        return userService.login(userMap, httpSession);
    }

    /**
     * 用户退出
     * @param httpRequest 请求
     * @return R<String>
     */
    @PostMapping("/loginout")
    public R<String> loginOut(HttpServletRequest httpRequest) {
        httpRequest.getSession().removeAttribute("user");
        return R.success("退出成功！");
    }

}
