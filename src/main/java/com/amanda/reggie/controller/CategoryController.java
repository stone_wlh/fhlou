package com.amanda.reggie.controller;

import com.amanda.reggie.common.R;
import com.amanda.reggie.entity.Category;
import com.amanda.reggie.service.CategoryService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author amanda
 * @Description 分类管理
 */
@Slf4j
@RestController
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    /**
     * 新增分类
     * insert into category () values ()
     * @param category 分类对象
     * @return R<String>
     */
    @PostMapping
    public R<String> save(@RequestBody Category category) {
        categoryService.save(category);
        return R.success("新增分类成功！");
    }

    /**
     * 分页查询
     * select * from category limit firstIndex, pageSize order by sort desc
     * @param page 页数
     * @param pageSize 条数
     * @return R<Page>
     */
    @GetMapping("/page")
    public R<Page> pageQuery(int page, int pageSize) {
        Page<Category> categoryPage = categoryService.pageQuery(page, pageSize);
        return R.success(categoryPage);
    }

    /**
     * 根据id删除分类
     * delete from category where id = ''
     * @param ids 分类id
     * @return R<String>
     */
    @DeleteMapping
    public R<String> deleteCategory(@RequestParam Long ids) {
        categoryService.remove(ids);
        return R.success("分类信息删除成功！");
    }

    /**
     * 根据id修改分类信息
     * update category set () values () where id = ''
     * @param category 分类项
     * @return R<String>
     */
    @PutMapping
    public R<String> updateCategory(@RequestBody Category category) {
        categoryService.updateById(category);
        return R.success("修改分类信息成功！");
    }

    /**
     * 查询分类数据列表，分类的回显
     * select * from category where type = ''
     * @param category 分类项
     * @return R<List<Category>>
     */
    @GetMapping("/list")
    public R<List<Category>> getList(Category category) {
        return R.success(categoryService.getList(category));
    }
}
