package com.amanda.reggie.controller;

import com.amanda.reggie.common.BaseContext;
import com.amanda.reggie.common.R;
import com.amanda.reggie.entity.AddressBook;
import com.amanda.reggie.service.AddressBookService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author amanda
 * @Description 地址簿管理
 */
@Slf4j
@RestController
@RequestMapping("/addressBook")
public class AddressBookController {

    @Autowired
    private AddressBookService addressBookService;

    /**
     * 查询指定用户的全部地址
     * @param addressBook 地址信息
     * @return R<List<AddressBook>>
     */
    @GetMapping("/list")
    public R<List<AddressBook>> list(AddressBook addressBook) {
        return R.success(addressBookService.listAll(addressBook));
    }

    /**
     * 新增地址
     * @param addressBook 地址信息
     * @return R<AddressBook>
     */
    @PostMapping
    public R<AddressBook> save(@RequestBody AddressBook addressBook) {
        // 当前用户
        addressBook.setUserId(BaseContext.getCurrentId());
        addressBookService.save(addressBook);
        return R.success(addressBook);
    }

    /**
     * 设置默认地址
     * @param addressBook 地址信息
     * @return R<AddressBook>
     */
    @PutMapping("default")
    public R<AddressBook> setDefault(@RequestBody AddressBook addressBook) {
        return R.success(addressBookService.setDefault(addressBook));
    }

    /**
     * 根据id查询地址
     * @param id id
     * @return R
     */
    @GetMapping("/{id}")
    public R get(@PathVariable Long id) {
        AddressBook addressBook = addressBookService.getById(id);
        if (addressBook != null) {
            return R.success(addressBook);
        } else {
            return R.error("没有找到该对象！");
        }
    }

    /**
     * 提交订单时，查询默认地址
     * @return R<AddressBook>
     */
    @GetMapping("default")
    public R<AddressBook> getDefault() {
        LambdaQueryWrapper<AddressBook> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(AddressBook::getUserId, BaseContext.getCurrentId());
        queryWrapper.eq(AddressBook::getIsDefault, 1);
        // SQL:select * from address_book where user_id = ? and is_default = 1
        AddressBook addressBook = addressBookService.getOne(queryWrapper);
        if (null == addressBook) {
            return R.error("没有找到该对象！");
        } else {
            return R.success(addressBook);
        }
    }

    /**
     * 修改地址
     * @param addressBook 地址信息
     * @return R<String>
     */
    @PutMapping
    public R<String> updateAddress(@RequestBody AddressBook addressBook) {
        if (null == addressBook) {
            return R.error("请检查地址信息是否正确！");
        }
        addressBookService.updateById(addressBook);
        return R.success("修改地址成功！");
    }

    /**
     * 删除地址
     * @param ids 地址id
     * @return R<String>
     */
    @DeleteMapping
    public R<String> deleteAddress(@RequestParam("ids") Long ids) {
        if (null == ids) {
            return R.error("操作异常，请联系管理员处理！");
        }
        addressBookService.removeById(ids);
        return R.success("删除地址成功！");
    }

}
