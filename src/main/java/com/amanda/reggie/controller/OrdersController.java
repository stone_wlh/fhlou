package com.amanda.reggie.controller;

import com.amanda.reggie.common.R;
import com.amanda.reggie.entity.Orders;
import com.amanda.reggie.service.OrdersService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @author amanda
 * @Description 订单管理
 */

@Slf4j
@RestController
@RequestMapping("/order")
public class OrdersController {

    @Autowired
    private OrdersService ordersService;

    /**
     * 订单分页查询
     * @param page 页码
     * @param pageSize 条数
     * @param number 订单号
     * @return R<Page>
     */
    @GetMapping("/page")
    public R<Page> pageQuery(int page, int pageSize, String number, String beginTime, String endTime) {
        return R.success(ordersService.pageQuery(page, pageSize, number, beginTime, endTime));
    }

    /**
     * 后台管理员更改订单状态
     * @param orderMap order数据
     * @return R<String>
     */
    @PutMapping
    public R<String> updateOrderStatus(@RequestBody Map<String,String> orderMap) {
        return ordersService.updateOrderStatus(orderMap);
    }

    /**
     * 提交订单
     * @param orders 订单数据
     * @return R<String>
     */
    @PostMapping("/submit")
    public R<String> submitOrders(@RequestBody Orders orders) {
        ordersService.submitOrders(orders);
        return R.success("下单成功！");
    }

    /**
     * 移动端订单分页查询
     * @param page 页码
     * @param pageSize 条数
     * @return R<Page>
     */
    @GetMapping("/userPage")
    public R<Page> pageQuery(int page, int pageSize) {
        return R.success(ordersService.pageQuery(page, pageSize));
    }

    /**
     * 再来一单
     * @param map 历史订单数据
     * @return R<String>
     */
    @PostMapping("/again")
    public R<String> againSubmit(@RequestBody Map<String, String> map) {
        ordersService.againSubmit(map);
        return R.success("再来一单成功！");
    }
}
