package com.amanda.reggie.controller;

import com.amanda.reggie.common.R;
import com.amanda.reggie.dto.DishDto;
import com.amanda.reggie.entity.Dish;
import com.amanda.reggie.service.DishService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author amanda
 * @Description 菜品管理
 */
@RestController
@RequestMapping("/dish")
@Slf4j
public class DishController {

    @Autowired
    private DishService dishService;

    /**
     * 新增菜品
     * @param dishDto 接收实体类
     * @return R<String>
     */
    @PostMapping
    public R<String> save(@RequestBody DishDto dishDto) {
        dishService.saveWithFlavor(dishDto);
        return R.success("新增菜品成功！");
    }

    /**
     * 菜品信息分页
     * @param page 页码
     * @param pageSize 条数
     * @return R<Page>
     */
    @GetMapping("/page")
    public R<Page> pageQuery(int page, int pageSize, String name) {
        Page<DishDto> dishPage = dishService.pageQuery(page, pageSize, name);
        return R.success(dishPage);
    }

    /**
     * 修改菜品时回显数据
     * @param id 菜品id
     * @return R<DishDto>
     */
    @GetMapping("/{id}")
    public R<DishDto> queryList(@PathVariable Long id) {
        DishDto dishDto = dishService.queryWithFlavorById(id);
        return R.success(dishDto);
    }

    /**
     * 修改菜品
     * @param dishDto 前端菜品数据
     * @return R<DishDto>
     */
    @PutMapping
    public R<String> updateDish(@RequestBody DishDto dishDto) {
        dishService.updateDish(dishDto);
        return R.success("修改菜品成功！");
    }

    /**
     * 删除菜品所有信息/批量删除
     * @param ids 菜品id
     * @return R<String>
     */
    @DeleteMapping
    public R<String> deleteDish(@RequestParam("ids") List<Long> ids) {
        dishService.remove(ids);
        return R.success("菜品数据删除成功！");
    }

    /**
     * 更改菜品状态/批量更改
     * @param status 状态码
     * @param ids 菜品id
     * @return R<String>
     */
    @PostMapping("/status/{status}")
    public R<String> updateDishStatus(@PathVariable("status") Integer status, @RequestParam List<Long> ids) {

        // 1、构造器
        LambdaQueryWrapper<Dish> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(ids != null, Dish::getId, ids);
        // 2、批量查询
        List<Dish> list = dishService.list(queryWrapper);
        int flag = 0;
        for (Dish dish : list) {
            if (null != dish) {
                dish.setStatus(status);
                flag = dish.getStatus();
                dishService.updateDishStatusById(dish);
            }
        }
        return R.success(flag == 0 ? "开启停售！" : "开启启售！");

    }

    /**
     * 操作套餐数据时，查询菜品数据
     * @param dish 菜品
     * @return R<List<Dish>>
     */
    /* 适用于后端业务
    @GetMapping("/list")
    public R<List<Dish>> listDish(Dish dish) {
        return dishService.listDish(dish);
    }*/
    /**
     * 操作套餐数据时，查询菜品数据（改造）
     * @param dish 菜品
     * @return R<List<DishDto>>
     */
    @GetMapping("/list")
    public R<List<DishDto>> listDish(Dish dish) {
        return dishService.listDish(dish);
    }

}
