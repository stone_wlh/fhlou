package com.amanda.reggie.dto;

import com.amanda.reggie.entity.Setmeal;
import com.amanda.reggie.entity.SetmealDish;
import lombok.Data;
import java.util.List;

/**
 * @author amanda
 * @Description 后端接收前端setmeal的dto
 */
@Data
public class SetmealDto extends Setmeal {

    private List<SetmealDish> setmealDishes;

    private String categoryName;
}
