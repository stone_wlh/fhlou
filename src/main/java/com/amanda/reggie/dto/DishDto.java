package com.amanda.reggie.dto;

import com.amanda.reggie.entity.Dish;
import com.amanda.reggie.entity.DishFlavor;
import lombok.Data;
import java.util.ArrayList;
import java.util.List;

/**
 * @author amanda
 * @Description 后端接收前端dish的dto
 */
@Data
public class DishDto extends Dish {

    private List<DishFlavor> flavors = new ArrayList<>();

    private String categoryName;

    private Integer copies;
}
