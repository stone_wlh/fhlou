package com.amanda.reggie.dto;

import com.amanda.reggie.entity.OrderDetail;
import com.amanda.reggie.entity.Orders;
import lombok.Data;
import java.util.List;

/**
 * @author amanda
 * @Description 后端接收前端orders的dto
 */
@Data
public class OrdersDto extends Orders {

    private String userName;

    private String phone;

    private String address;

    private String consignee;

    private List<OrderDetail> orderDetails;
	
}
