package com.amanda.reggie.filter;

import com.alibaba.fastjson.JSON;
import com.amanda.reggie.common.BaseContext;
import com.amanda.reggie.common.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.AntPathMatcher;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author amanda
 * @Description 自定义过滤器
 * 检查用户是否完成登录
 */
@Slf4j
@WebFilter(filterName = "loginCheckFilter", urlPatterns = "/*")
public class LoginCheckFilter implements Filter {

    // 路径匹配器，支持通配符
    public static final AntPathMatcher PATH_MATCHER = new AntPathMatcher();

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        // 1、获取本次请求URI
        String requestURI = request.getRequestURI();
        log.info("拦截到请求：{}", requestURI);
        // 1.1 定义不需要处理的路径
        String[] urls = new String[] {
                "/employee/login",
                "/employee/logout",
                "/backend/**",
                "/front/**",
                "/user/sendMsg",
                "/user/login"
        };
        // 2、判断本次请求是否需要处理
        boolean check = check(urls, requestURI);
        // 3、如果不需要处理，放行
        if (check) {
            log.info("本次请求{}不需要处理，放行！", requestURI);
            filterChain.doFilter(request, response);
            return;
        }
        // 4.1 判断后台登录状态
        if (null != (request.getSession().getAttribute("employee"))) {
            // 已登录，放行
            log.info("员工用户已登录，用户id为：{}", request.getSession().getAttribute("employee"));
            Long employId = (Long) request.getSession().getAttribute("employee");
            // 同一线程id
            BaseContext.setCurrentId(employId);
            filterChain.doFilter(request, response);
            return;
        }
        // 4.2 判断移动端登录状态
        if(null != (request.getSession().getAttribute("user"))) {
            // 已登录，放行
            log.info("大众用户已登录，用户id为：{}",request.getSession().getAttribute("user"));
            // 把用户id存储到本地的threadLocal
            Long userId = (Long) request.getSession().getAttribute("user");
            BaseContext.setCurrentId(userId);
            filterChain.doFilter(request, response);
            return;
        }
        // 未登录拦截，通过输出流的方式向客户端页面相应数据
        log.info("用户未登录！");
        response.getWriter().write(JSON.toJSONString(R.error("NOTLOGIN")));
        return;
    }

    /**
     * 路径匹配，检查本次请求是否符合要求
     * @param urls 规则url
     * @param requestURI 请求url
     * @return boolean
     */
    public boolean check(String[] urls, String requestURI) {
        for (String url : urls) {
            boolean match = PATH_MATCHER.match(url, requestURI);
            if (match) {
                return true;
            }
        }
        return false;
    }

}
