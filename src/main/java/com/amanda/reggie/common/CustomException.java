package com.amanda.reggie.common;

/**
 * @author amanda
 * @Description 自定义异常处理
 * 返回自定义异常信息
 */
public class CustomException extends RuntimeException{
    public CustomException(String message) {
        super(message);
    }
}
