package com.amanda.reggie.utils;

import com.alibaba.fastjson.JSONException;
import com.github.qcloudsms.SmsSingleSender;
import com.github.qcloudsms.SmsSingleSenderResult;
import com.github.qcloudsms.httpclient.HTTPException;

import java.io.IOException;

/**
 * @author amanda
 * @Description 短信发送工具类
 */
public class SMSUtils {

	/**
	 * 发送短信，使用腾讯云服务
	 * @param phoneNumbers 手机号
	 * @param code 验证码
	 */
	public static void sendMessage(String phoneNumbers,String code) {
		// 短信应用 SDK AppID
		int appId = 1400720000;
		// 短信应用 SDK AppKey
		String appKey = "AppKey";
		// 模板管理 ID，需要在短信应用中申请
		int templateId = 1511000;
		// 签名管理 smsSign
		String smsSign = "smsSign";
		try {
			String[] params = {code, "1"};	// code：验证码，1：分钟时长
			SmsSingleSender sender = new SmsSingleSender(appId, appKey);
			SmsSingleSenderResult result = sender.sendWithParam("86", phoneNumbers,
					templateId, params, smsSign, "", "");
			System.out.println(result);
		} catch (HTTPException | IOException | JSONException e) {
			// HTTP 响应码错误、JSON 解析错误、网络 IO错误
			e.printStackTrace();
		}
	}

}
