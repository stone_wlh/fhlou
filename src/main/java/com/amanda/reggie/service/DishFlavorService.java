package com.amanda.reggie.service;

import com.amanda.reggie.entity.DishFlavor;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author amanda
 */
public interface DishFlavorService extends IService<DishFlavor> {
}
