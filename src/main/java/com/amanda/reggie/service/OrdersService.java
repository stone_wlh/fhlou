package com.amanda.reggie.service;

import com.amanda.reggie.common.R;
import com.amanda.reggie.dto.OrdersDto;
import com.amanda.reggie.entity.Orders;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * @author amanda
 */
public interface OrdersService extends IService<Orders> {
    // 后台订单分页查询
    public Page<OrdersDto> pageQuery(int page, int pageSize, String number, String beginTime, String endTime);
    // 更改订单状态
    public R<String> updateOrderStatus(Map<String, String> orderMap);

    // 移动端提交订单
    public void submitOrders(Orders orders);
    // 移动端订单分页查询
    public Page<OrdersDto> pageQuery(int page, int pageSize);
    // 再来一单
    public void againSubmit(Map<String, String> map);
}
