package com.amanda.reggie.service;

import com.amanda.reggie.entity.AddressBook;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @author amanda
 */
public interface AddressBookService extends IService<AddressBook> {
    // 获取用户全部地址信息
    public List<AddressBook> listAll(AddressBook addressBook);
    // 设置当前地址为默认地址
    public AddressBook setDefault(AddressBook addressBook);
}
