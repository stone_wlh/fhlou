package com.amanda.reggie.service;

import com.amanda.reggie.entity.SetmealDish;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author amanda
 */
public interface SetmealDishService extends IService<SetmealDish> {
}
