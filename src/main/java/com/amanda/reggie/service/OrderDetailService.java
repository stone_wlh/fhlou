package com.amanda.reggie.service;

import com.amanda.reggie.entity.OrderDetail;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @author amanda
 */
public interface OrderDetailService extends IService<OrderDetail> {
    public List<OrderDetail> getByOrderId(Long ordersId);
}
