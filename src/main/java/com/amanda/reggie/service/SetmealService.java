package com.amanda.reggie.service;

import com.amanda.reggie.dto.DishDto;
import com.amanda.reggie.dto.SetmealDto;
import com.amanda.reggie.entity.Setmeal;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @author amanda
 */
public interface SetmealService extends IService<Setmeal> {
    // 新增套餐，同时保存菜品的信息
    public void saveSetmealWithDish(SetmealDto setmealDto);
    // 删除套餐，同时清除菜品的信息
    public void removeSetmeal(List<Long> ids);
    // 直接更改套餐的状态
    public void updateSetmealStatus(Integer status, List<Long> ids);
    // 修改套餐时回显数据
    public SetmealDto querySetmeal(Long id);
    // 修改套餐数据
    public void updateSetmeal(SetmealDto setmealDto);
    // 后台套餐的分页查询
    public Page<SetmealDto> pageSetmeal(int page, int pageSize, String name);

    // 移动端查询套餐
    public List<Setmeal> listSetmeal(Setmeal setmeal);
    // 移动端查询套餐中的菜品信息
    public List<DishDto> listDishOnSetmeal(Long setmealId);
}
