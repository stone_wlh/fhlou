package com.amanda.reggie.service;

import com.amanda.reggie.common.R;
import com.amanda.reggie.entity.ShoppingCart;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @author amanda
 */
public interface ShoppingCartService extends IService<ShoppingCart> {
    // 添加美食到购物车
    public ShoppingCart addShoppingCart(ShoppingCart shoppingCart);
    // 查询购物车数据
    public List<ShoppingCart> listShoppingCart();
    // 减少当前购物车的美食
    public R<String> subDishOrSetmeal(ShoppingCart shoppingCart);
    // 清空当前用户购物车
    public void cleanShoppingCart();
}
