package com.amanda.reggie.service;

import com.amanda.reggie.common.R;
import com.amanda.reggie.dto.DishDto;
import com.amanda.reggie.entity.Dish;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @author amanda
 */
public interface DishService extends IService<Dish> {
    // 新增菜品，同时插入菜品对应口味数据，dish和dish_flavor
    public void saveWithFlavor(DishDto dishDto);
    // 修改菜品时，回显数据，dish和dish_flavor
    public DishDto queryWithFlavorById(Long id);
    // 更新菜品信息，同时操作口味数据，dish和dish_flavor
    public void updateDish(DishDto dishDto);
    // 删除菜品信息并且将所有关联口味清空，但关联套餐异常
    public void remove(List<Long> ids);
    // 菜品的起售或者停售
    public void updateDishStatusById(Dish dish);
    // 菜品的分页查询
    public Page<DishDto> pageQuery(int page, int pageSize, String name);

    // 添加套餐时，查询菜品数据
    //public R<List<Dish>> listDish(Dish dish);
    // 改造
    public R<List<DishDto>> listDish(Dish dish);
}
