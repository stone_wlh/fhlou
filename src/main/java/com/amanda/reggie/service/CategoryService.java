package com.amanda.reggie.service;

import com.amanda.reggie.entity.Category;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @author amanda
 */
public interface CategoryService extends IService<Category> {
    // 根据id删除分类
    public void remove(Long id);
    // 分类分页查询
    public Page<Category> pageQuery(int page, int pageSize);
    // 分类的回显
    public List<Category> getList(Category category);
}
