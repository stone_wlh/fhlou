package com.amanda.reggie.service.impl;

import com.amanda.reggie.entity.DishFlavor;
import com.amanda.reggie.mapper.DishFlavorMapper;
import com.amanda.reggie.service.DishFlavorService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author amanda
 * @Description 菜品管理业务实现
 */
@Service
@Slf4j
public class DishFlavorServiceImpl extends ServiceImpl<DishFlavorMapper, DishFlavor> implements DishFlavorService {
}
