package com.amanda.reggie.service.impl;

import com.amanda.reggie.common.CustomException;
import com.amanda.reggie.dto.DishDto;
import com.amanda.reggie.dto.SetmealDto;
import com.amanda.reggie.entity.Category;
import com.amanda.reggie.entity.Dish;
import com.amanda.reggie.entity.Setmeal;
import com.amanda.reggie.entity.SetmealDish;
import com.amanda.reggie.mapper.SetmealMapper;
import com.amanda.reggie.service.CategoryService;
import com.amanda.reggie.service.DishService;
import com.amanda.reggie.service.SetmealDishService;
import com.amanda.reggie.service.SetmealService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author amanda
 * @Description 套餐业务实现
 */
@Service
@Slf4j
public class SetmealServiceImpl extends ServiceImpl<SetmealMapper, Setmeal> implements SetmealService {

    @Autowired
    private SetmealDishService setmealDishService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private SetmealService setmealService;

    @Autowired
    private DishService dishService;

    /**
     * 新建套餐
     * @param setmealDto 套餐数据
     */
    @Override
    @Transactional
    public void saveSetmealWithDish(SetmealDto setmealDto) {
        // 1、保存套餐的基本信息，setmeal_insert
        this.save(setmealDto);
        // 2、封装
        List<SetmealDish> setmealDishes = setmealDto.getSetmealDishes();
        setmealDishes.stream().map((dish) ->{
            dish.setSetmealId(setmealDto.getId());
            return dish;
        }).collect(Collectors.toList());
        // 3、保存菜品信息，setmeal_dish_inset
        setmealDishService.saveBatch(setmealDishes);
    }

    /**
     * 删除套餐
     * @param ids 套餐id
     */
    @Override
    @Transactional
    public void removeSetmeal(List<Long> ids) {
        // 1、查询套餐状态，能否删除
        LambdaQueryWrapper<Setmeal> setmealQueryWrapper = new LambdaQueryWrapper<>();
        setmealQueryWrapper.in(null != ids, Setmeal::getId, ids);
        setmealQueryWrapper.eq(Setmeal::getStatus, 1);
        int count = this.count(setmealQueryWrapper);
        if (count > 0) {
            throw new CustomException("该套餐正在售卖中，不能删除！");
        }
        // 2、先删除setmeal_dish表
        LambdaQueryWrapper<SetmealDish> setmealDishWrapper = new LambdaQueryWrapper<>();
        setmealDishWrapper.in(SetmealDish::getSetmealId, ids);
        setmealDishService.remove(setmealDishWrapper);
        // 3、后删除setmeal表
        this.removeByIds(ids);
    }

    /**
     * 修改套餐的状态
     * @param status 状态码
     * @param ids 套餐id
     */
    @Override
    public void updateSetmealStatus(Integer status, List<Long> ids) {
        // 1、构造器
        LambdaQueryWrapper<Setmeal> setmealQueryWrapper = new LambdaQueryWrapper<>();
        setmealQueryWrapper.in(ids != null, Setmeal::getId, ids);
        // 2、查询集合
        List<Setmeal> list = this.list(setmealQueryWrapper);
        for (Setmeal setmeal : list) {
            if (null != setmeal) {
                setmeal.setStatus(status);
                // 3、批量处理，不需要考虑其他表
                this.updateById(setmeal);
            }
        }
    }

    /**
     * 修改套餐时回显数据
     * @param id 套餐id
     * @return SetmealDto
     */
    @Override
    public SetmealDto querySetmeal(Long id) {
        // 1、查询套餐基本信息
        Setmeal setmeal = this.getById(id);
        SetmealDto setmealDto = new SetmealDto();
        // 2、对象拷贝
        BeanUtils.copyProperties(setmeal, setmealDto);
        // 3、对应菜品
        LambdaQueryWrapper<SetmealDish> setmealDishWrapper = new LambdaQueryWrapper<>();
        setmealDishWrapper.eq(id != null, SetmealDish::getSetmealId, id);
        List<SetmealDish> setmealDishList = setmealDishService.list(setmealDishWrapper);
        setmealDto.setSetmealDishes(setmealDishList);
        return setmealDto;
    }

    /**
     * 修改套餐数据
     * @param setmealDto 数据
     */
    @Override
    @Transactional
    public void updateSetmeal(SetmealDto setmealDto) {
        // 1、清除该套餐中的菜品数据
        LambdaQueryWrapper<SetmealDish> setmealDishWrapper = new LambdaQueryWrapper<>();
        setmealDishWrapper.eq(SetmealDish::getSetmealId, setmealDto.getId());
        setmealDishService.remove(setmealDishWrapper);
        // 2、重新添加菜品数据
        List<SetmealDish> setmealDishes = setmealDto.getSetmealDishes();
        // 3、处理setmeal_id
        setmealDishes.stream().map((setmealDish) -> {
            setmealDish.setSetmealId(setmealDto.getId());
            return setmealDish;
        }).collect(Collectors.toList());
        setmealDishService.saveBatch(setmealDishes);
        // 4、更新setmeal表基本信息
        this.updateById(setmealDto);
    }

    /**
     * 套餐的分页查询
     */
    @Override
    public Page<SetmealDto> pageSetmeal(int page, int pageSize, String name) {
        // 1、构造分页构造器
        Page<Setmeal> pageInfo = new Page<>(page, pageSize);
        Page<SetmealDto> setmealDtoPage = new Page<>();
        // 2、构造条件构造器
        LambdaQueryWrapper<Setmeal> dishQueryWrapper = new LambdaQueryWrapper<>();
        // 3、过滤
        dishQueryWrapper.like(name != null, Setmeal::getName, name);
        // 4、排序
        dishQueryWrapper.orderByDesc(Setmeal::getUpdateTime);
        // 5、查询
        setmealService.page(pageInfo, dishQueryWrapper);
        // 6、对象拷贝
        BeanUtils.copyProperties(pageInfo, setmealDtoPage, "records");
        // 7、处理records
        List<Setmeal> records = pageInfo.getRecords();
        List<SetmealDto> list = records.stream().map((setmeal) -> {
            SetmealDto setmealDto = new SetmealDto();
            // 拷贝
            BeanUtils.copyProperties(setmeal, setmealDto);
            Long categoryId = setmeal.getCategoryId(); // 分类id
            Category category = categoryService.getById(categoryId); // 分类对象
            if (null != category) {
                String categoryName = category.getName();
                setmealDto.setCategoryName(categoryName);
            }
            return setmealDto;
        }).collect(Collectors.toList());
        // 8、封装
        return setmealDtoPage.setRecords(list);
    }

    /**
     * 移动端查询套餐
     */
    @Override
    public List<Setmeal> listSetmeal(Setmeal setmeal) {
        LambdaQueryWrapper<Setmeal> setmealWrapper = new LambdaQueryWrapper<>();
        setmealWrapper.eq(setmeal.getCategoryId() != null, Setmeal::getCategoryId, setmeal.getCategoryId());
        setmealWrapper.eq(setmeal.getStatus() != null, Setmeal::getStatus, setmeal.getStatus());
        setmealWrapper.orderByDesc(Setmeal::getUpdateTime);
        return setmealService.list(setmealWrapper);
    }

    /**
     * 移动端查询套餐中的菜品信息
     */
    @Override
    public List<DishDto> listDishOnSetmeal(Long setmealId) {
        // 1、构造器
        LambdaQueryWrapper<SetmealDish> setmealDishWrapper = new LambdaQueryWrapper<>();
        setmealDishWrapper.eq(SetmealDish::getSetmealId, setmealId);
        // 2、查询套餐的菜品
        List<SetmealDish> setmealDishes = setmealDishService.list(setmealDishWrapper);
        return setmealDishes.stream().map((setmealDish) -> {
            DishDto dishDto = new DishDto();
            // 2、拷贝数据
            BeanUtils.copyProperties(setmealDish, dishDto);
            Long dishId = setmealDish.getDishId();
            Dish dish = dishService.getById(dishId);
            BeanUtils.copyProperties(dish, dishDto);
            return dishDto;
        }).collect(Collectors.toList());
    }
}
