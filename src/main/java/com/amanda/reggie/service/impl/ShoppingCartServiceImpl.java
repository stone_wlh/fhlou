package com.amanda.reggie.service.impl;

import com.amanda.reggie.common.BaseContext;
import com.amanda.reggie.common.R;
import com.amanda.reggie.entity.ShoppingCart;
import com.amanda.reggie.mapper.ShoppingCartMapper;
import com.amanda.reggie.service.ShoppingCartService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author amanda
 * @Description 购物业务实现
 */
@Slf4j
@Service
public class ShoppingCartServiceImpl extends ServiceImpl<ShoppingCartMapper, ShoppingCart> implements ShoppingCartService {

    @Autowired
    private ShoppingCartService shoppingCartService;

    @Override
    public ShoppingCart addShoppingCart(ShoppingCart shoppingCart) {
        // 1、设置用户id，指定当前用户的购物车数据
        Long currentId = BaseContext.getCurrentId();
        shoppingCart.setUserId(currentId);
        // 2、查询当前菜品或者套餐是否已经在购物车内
        Long dishId = shoppingCart.getDishId();
        // 3、封装对象
        LambdaQueryWrapper<ShoppingCart> shoppingCartWrapper = new LambdaQueryWrapper<>();
        shoppingCartWrapper.eq(ShoppingCart::getUserId, currentId);
        if (dishId != null) {
            // 3、1添加到购物车的是菜品
            shoppingCartWrapper.eq(ShoppingCart::getDishId, dishId);
        }else {
            // 3、2添加到购物车的是菜品套餐
            shoppingCartWrapper.eq(ShoppingCart::getSetmealId, shoppingCart.getSetmealId());
        }
        // SQL：select * from shopping_cart where user_id = ? and dish_id/setmeal_id = ?
        ShoppingCart serviceOne = shoppingCartService.getOne(shoppingCartWrapper);
        if (null != serviceOne) {
            // 4、存在，+1
            Integer number = serviceOne.getNumber();
            serviceOne.setNumber(number + 1);
            serviceOne.setCreateTime(LocalDateTime.now());
            shoppingCartService.updateById(serviceOne);
        }else {
            // 5、不存在，默认为1
            shoppingCart.setNumber(1);
            shoppingCart.setCreateTime(LocalDateTime.now());
            shoppingCartService.save(shoppingCart);
            serviceOne = shoppingCart;
        }
        return serviceOne;
    }

    /**
     * 查看购物车数据
     */
    @Override
    public List<ShoppingCart> listShoppingCart() {
        LambdaQueryWrapper<ShoppingCart> shoppingCartWrapper = new LambdaQueryWrapper<>();
        shoppingCartWrapper.eq(ShoppingCart::getUserId, BaseContext.getCurrentId());
        shoppingCartWrapper.orderByDesc(ShoppingCart::getCreateTime);
        return shoppingCartService.list(shoppingCartWrapper);
    }

    /**
     * 减少当前购物车的美食
     */
    @Override
    @Transactional
    public R<String> subDishOrSetmeal(ShoppingCart shoppingCart) {
        LambdaQueryWrapper<ShoppingCart> shoppingCartWrapper = new LambdaQueryWrapper<>();
        // 1、判断操作的是否为菜品
        Long dishId = shoppingCart.getDishId();
        if (null != dishId) {
            shoppingCartWrapper.eq(ShoppingCart::getDishId, dishId);
            // 1.1 操作菜品数据
            return this.dishOrSetmealJudge(shoppingCartWrapper);
        }
        // 2、判断操作的是否为套餐
        Long setmealId = shoppingCart.getSetmealId();
        if (null != setmealId) {
            shoppingCartWrapper.eq(ShoppingCart::getSetmealId, setmealId);
            // 2.1 操作套餐数据
            return this.dishOrSetmealJudge(shoppingCartWrapper);
        }
        return R.error("操作异常！");
    }
    // 操作菜品或者套餐数据
    private R<String> dishOrSetmealJudge(LambdaQueryWrapper<ShoppingCart> shoppingCartWrapper) {
        ShoppingCart cartService = shoppingCartService.getOne(shoppingCartWrapper);
        // 更新
        Integer number = cartService.getNumber();
        if (number != 0) {
            cartService.setNumber(number-1);
            if (cartService.getNumber() > 0) {
                shoppingCartService.updateById(cartService);
                return R.success("减少美食成功！");
            }else {
                shoppingCartService.removeById(cartService.getId());
                return R.success("清空美食成功！");
            }
        }
        return R.error("操作异常！");
    }

    /**
     * 清空用户购物车
     */
    @Override
    public void cleanShoppingCart() {
        LambdaQueryWrapper<ShoppingCart> shoppingCartWrapper = new LambdaQueryWrapper<>();
        shoppingCartWrapper.eq(ShoppingCart::getUserId, BaseContext.getCurrentId());
        shoppingCartService.remove(shoppingCartWrapper);
    }
}
