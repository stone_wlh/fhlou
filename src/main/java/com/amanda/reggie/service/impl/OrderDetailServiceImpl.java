package com.amanda.reggie.service.impl;

import com.amanda.reggie.entity.OrderDetail;
import com.amanda.reggie.mapper.OrderDetailMapper;
import com.amanda.reggie.service.OrderDetailService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author amanda
 * @Description 订单内容业务实现
 */
@Slf4j
@Service
public class OrderDetailServiceImpl extends ServiceImpl<OrderDetailMapper, OrderDetail> implements OrderDetailService {
    public List<OrderDetail> getByOrderId(Long id) {
        LambdaQueryWrapper<OrderDetail> orderDetailLambdaQueryWrapper = new LambdaQueryWrapper<>();
        orderDetailLambdaQueryWrapper.eq(OrderDetail::getOrderId, id);
        return this.list(orderDetailLambdaQueryWrapper);
    }
}
