package com.amanda.reggie.service.impl;

import com.amanda.reggie.entity.SetmealDish;
import com.amanda.reggie.mapper.SetmealDishMapper;
import com.amanda.reggie.service.SetmealDishService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author amanda
 * @Description 套餐菜品管理业务实现
 */
@Service
@Slf4j
public class SetmealDishServiceImpl extends ServiceImpl<SetmealDishMapper, SetmealDish> implements SetmealDishService {
}
