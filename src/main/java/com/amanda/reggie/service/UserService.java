package com.amanda.reggie.service;

import com.amanda.reggie.common.R;
import com.amanda.reggie.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * @author amanda
 */
public interface UserService extends IService<User> {
    // 发送短信
    public R<String> sendMsg(User user, HttpSession httpSession);
    // 登录
    public R<User> login(Map userMap, HttpSession httpSession);
}
