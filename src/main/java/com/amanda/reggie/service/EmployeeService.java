package com.amanda.reggie.service;

import com.amanda.reggie.common.R;
import com.amanda.reggie.entity.Employee;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import javax.servlet.http.HttpServletRequest;

/**
 * @author amanda
 * 使用MP处理简单业务
 */
public interface EmployeeService extends IService<Employee> {
    // 员工登录
    public R<Employee> login(HttpServletRequest request, Employee employee);
    // 员工分页查询
    public Page<Employee> pageQuery(int page, int pageSize, String name);
}
