// 员工登录
function loginApi(data) {
  return $axios({
    'url': '/employee/login',
    'method': 'post',
    data
  })
}

// 员工退出登录
function logoutApi(){
  return $axios({
    'url': '/employee/logout',
    'method': 'post',
  })
}

// 移动端发送验证码
function sendMsgApi(data){
  return $axios({
    'url':'/user/sendMsg',
    'method':'post',
    data
  })
}
